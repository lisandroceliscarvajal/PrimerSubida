﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registar_negocio.aspx.cs" Inherits="ComidasRapidas.paginas.registar_negocio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>Menu</title>
    <link rel="stylesheet" href="../estilos/menu.css" />   
    <link rel="stylesheet" href="../estilos/registrar_negocio.css" />  
     <script src="../Scripts/validaciones.js"></script> 
    <script src="http://code.jquery.com/jquery-latest.js"></script>

    <script type="text/javascript">  
  
        function showimagepreview(input) {  
  
            if (input.files && input.files[0]) {  
                var reader = new FileReader();  
                reader.onload = function (e) {  
  
                    document.getElementsByTagName("img")[0].setAttribute("src", e.target.result);  
                }  
                reader.readAsDataURL(input.files[0]);  
            }  
        }  
  
    </script>  


</head>
<header>
    <div class="wrapper">
        <div class="logo">ComidasRápidas</div>

        <nav>
            <a href="index.aspx">Inicio</a>
            <a href="registrar_cliente.aspx">Registrarme</a>
            <a href="iniciar_sesion.aspx">Iniciar Sesión</a>
             <a href="contacto.aspx">Contacto</a>
        </nav>
    </div>

</header>
<body style="background: #ededed">
    <form id="form1" runat="server">
    <div class="general" style="background: #ededed">
        <div class="centrado">
             <h1> Datos del negocio</h1>
            <asp:TextBox ID="txt_numero" CssClass="textbox" placeholder="Número" required="required" onKeyPress="return soloNumeros(event)" runat="server"></asp:TextBox>
             <asp:TextBox ID="txt_nombre" CssClass="textbox" required="required"  placeholder="Nombre" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_direccion" CssClass="textbox" placeholder="Dirección" required="required" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_telefono" placeholder="Telefono" CssClass="textbox" required="required" onblur="validacion(this)" onKeyPress="return soloNumeros(event)"  runat="server"></asp:TextBox>            
            <asp:Button ID="btn_registrar" CssClass="boton" runat="server" Text="Registrar" />      
        </div>
       <div class="cargaimagen">
           <h2>Imagen del negocio</h2>
           <img id="img" class="imagen" alt=""  />  
           <asp:FileUpload ID="FileUpload1" CssClass="fileuplo" required="required" runat="server"   onchange="showimagepreview(this)" />
            
       </div>

    </div>
        <div class="footer">
            <div class="footer_hijo">
                Copyright 2015 - Desarrollado por Lisandro Celis
            </div>
            
        </div>
    </form>
</body>
</html>
