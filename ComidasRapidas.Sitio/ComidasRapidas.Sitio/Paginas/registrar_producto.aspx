﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registrar_producto.aspx.cs" MasterPageFile="/sesion.Master" Inherits="ComidasRapidas.paginas.registrar_producto" %>


<asp:Content ID="Content2" ContentPlaceHolderID="central" runat="Server">
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

        <asp:View ID="View1" runat="server"> 
               
            <head>                
                <link rel="stylesheet" href="../estilos/menu.css" />
                <link rel="stylesheet" href="../estilos/registrar_negocio.css" />
                <script src="../Scripts/validaciones.js"></script>
                <script src="http://code.jquery.com/jquery-latest.js"></script>

                <script type="text/javascript">
                    function showimagepreview(input) {

                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {

                                document.getElementsByTagName("img")[0].setAttribute("src", e.target.result);
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>

            </head>

            <body style="background: #ededed">
               
                    <div class="general" style="background: #ededed">
                        <div class="centrado">
                            <h1>Datos del Producto</h1>
                            <asp:TextBox ID="txt_nombre" CssClass="textbox" placeholder="Nombre del Producto" required="required" runat="server"></asp:TextBox>

                            <asp:TextBox Style="height: 80px" ID="txt_descripcion" CssClass="textbox" required="required" placeholder="Descripción del producto" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txt_precio" CssClass="textbox" placeholder="Precio" onKeyPress="return soloNumeros(event)" required="required" runat="server"></asp:TextBox>


                            <asp:Button ID="btn_registrar" CssClass="boton" runat="server" Text="Registrar" />
                        </div>
                        <div class="cargaimagen">
                            <h2>Imagen del Producto</h2>
                            <img id="img" class="imagen" alt="" />
                            <asp:FileUpload ID="FileUpload1" CssClass="fileuplo" required="required" runat="server" onchange="showimagepreview(this)" />

                        </div>

                    </div>
                    <div class="footer">
                        <div class="footer_hijo">
                            Copyright 2015 - Desarrollado por Lisandro Celis
                        </div>

                    </div>
              
            </body>

        </asp:View>

    </asp:MultiView>
</asp:Content>



