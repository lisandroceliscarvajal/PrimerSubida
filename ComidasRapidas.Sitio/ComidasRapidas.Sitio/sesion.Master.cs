﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ComidasRapidas.Negocio;

namespace ComidasRapidas.Sitio
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["usuario"] == null)
                {
                    Response.Redirect("iniciar_sesion.aspx");
                }
                if (Session["usuario"] != null)
                {
                    this.Menu();
                }
            }

        }

        private void Menu()
        {
            int rol = int.Parse(Session["id_rol"].ToString());
            menu_sesion menuse= new menu_sesion();
            DataTable menu = menuse.consultar_menu_sesion(rol);

            String html ="<div class=logo>ComidasRápidas</div>";
            html += "<nav>";
           
            for (int i = 0; i < menu.Rows.Count; i++)
            {               
                html += "<a href=" + menu.Rows[i]["url_menu"].ToString() + "> " + menu.Rows[i]["nombre_menu"].ToString() + "</a>";               
              
            }
            
            html += "</nav>";
            wrapper.InnerHtml = html;
        }
    }
}