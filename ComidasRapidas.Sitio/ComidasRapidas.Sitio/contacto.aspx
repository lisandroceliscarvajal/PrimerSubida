﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contacto.aspx.cs" Inherits="ComidasRapidas.contacto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>Menu</title>
    <link rel="stylesheet" href="estilos/menu.css" />
    <link rel="stylesheet" href="estilos/contacto.css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<header>
    <div class="wrapper">
        <div class="logo">ComidasRápidas</div>

        <nav>
            <a href="index.aspx">Inicio</a>
            <a href="registrar_cliente.aspx">Registrarme</a>
            <a href="iniciar_sesion.aspx">Iniciar Sesión</a>
            <a href="contacto.aspx">Contacto</a>
        </nav>
    </div>

</header>
<body style="background: #ededed">
   
        <div class="general">
            <div class="centrado">
                <h1>Contacto</h1>
                <asp:Label ID="lbl_telefono" CssClass="encabezado" runat="server" Text="Teléfono:"></asp:Label>
                <p><asp:Label ID="lbl_numero" CssClass="subencabezado" runat="server" Text="Local: "></asp:Label>4357023</p>
                <p> <asp:Label ID="Label1" CssClass="subencabezado" runat="server" Text="Celular: "></asp:Label>3102246776</p>
                <asp:Label ID="Label2" CssClass="encabezado" runat="server" Text="Email:"></asp:Label>
                <p><asp:Label ID="Label3" CssClass="subencabezado" runat="server" Text="Soporte: "></asp:Label><a href="mailto:Support@example.com">Support@example.com</a></p>
                <p> <asp:Label ID="Label4" CssClass="subencabezado" runat="server" Text="Ventas: "></asp:Label><a href="mailto:Support@example.com">Marketing@example.com</a></p>
                 <asp:Label ID="Label5" CssClass="encabezado" runat="server" Text="Dirección:"></asp:Label>
                <p>Calle Falsa #123 </p>
            </div>
        </div>
        <div class="footer">
            <div class="footer_hijo">
                Copyright 2015 - Desarrollado por Lisandro Celis
            </div>
        </div>
</body>
</html>
