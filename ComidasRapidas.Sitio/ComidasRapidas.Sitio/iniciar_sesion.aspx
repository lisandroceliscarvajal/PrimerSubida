﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iniciar_sesion.aspx.cs" Inherits="ComidasRapidas.iniciar_sesion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>Menu</title>
    <link rel="stylesheet" href="estilos/menu.css" />
    <link rel="stylesheet" href="estilos/iniciar_sesion.css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>function visible() {
   
         var elemento = document.getElementById("error");
         elemento.style.display = 'block';
                }
    </script>
</head>
<header>
    <div class="wrapper">
        <div class="logo">ComidasRápidas</div>

        <nav>
            <a href="index.aspx">Inicio</a>
            <a href="registrar_cliente.aspx">Registrarme</a>
            <a href="iniciar_sesion.aspx">Iniciar Sesión</a>
            <a href="contacto.aspx">Contacto</a>
        </nav>
    </div>

</header>
<body style="background: #ededed">
    <form id="form1" runat="server">
        <div class="general" style="background: #ededed">
            <div class="error" id="error">
                Usuario o clave no válidos
            </div>
            <div class="centrado">
                <h1>Iniciar sesión</h1>
                <asp:TextBox ID="txt_usuario" CssClass="textbox" required="required" placeholder="Usuario" runat="server"></asp:TextBox>
                <asp:TextBox ID="txt_contrasenia" CssClass="textbox" required="required" type="password" placeholder="Clave" runat="server"></asp:TextBox>
                <asp:Button ID="btn_ingresar" CssClass="boton" runat="server" Text="Ingresar" OnClick="btn_ingresar_Click" />
                <p>
                    <a href="registrar_cliente.aspx">Registrarse,</a> si no tiene cuenta
                </p>



            </div>


        </div>
        <div class="footer">
            <div class="footer_hijo">
                Copyright 2015 - Desarrollado por Lisandro Celis
            </div>

        </div>
    </form>
</body>
</html>
