﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

namespace ComidasRapidas.Negocio
{
    class Conexion
    {
        private static MySqlConnection conexion = new MySqlConnection(ConfigurationManager.ConnectionStrings["conexion_mysql"].ConnectionString);


        private static bool Conectar()
        {
            try
            {

                conexion.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void Desconectar()
        {
            conexion.Close();
        }
        public static int EjecutarOperacion(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando)
        {
            if (Conectar())
            {
                MySqlCommand comando = new MySqlCommand();
                comando.CommandText = sentencia;
                comando.CommandType = TipoComando;
                comando.Connection = conexion;
                foreach (MySqlParameter parametro in ListaParametros)
                {
                    comando.Parameters.Add(parametro);
                }
                comando.ExecuteNonQuery();
                Desconectar();
                return 1;
            }
            else
            {
                throw new Exception("No ha sido posible conectarse a la Base de Datos");
            }
        }

        public static DataTable EjecutarConsulta(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando)
        {
            try
            {
                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(sentencia, conexion);
                adaptador.SelectCommand.CommandType = TipoComando;

                foreach (MySqlParameter parametro in ListaParametros)
                {
                    adaptador.SelectCommand.Parameters.Add(parametro);
                }
                DataSet resultado = new DataSet();
                adaptador.Fill(resultado);

                return resultado.Tables[0];

            }
            catch (Exception exe)
            {
                return null;
            }

        }

        public static void EjecutarTransaccion(List<string> Sentencia)//Metodo para ejecutar Select 
        {

            if (Conectar())
            {
                MySqlTransaction transa = conexion.BeginTransaction();
                MySqlCommand mySqlCommand;

                try
                {
                    for (int i = 0; i < Sentencia.Count; i++)
                    {
                        if (Sentencia[i].Length > 0)
                        {
                            mySqlCommand = new MySqlCommand(Sentencia[i], conexion);
                            mySqlCommand.Transaction = transa;
                            mySqlCommand.ExecuteNonQuery();
                        }
                    }
                    transa.Commit();
                    //   return true;
                }
                catch
                {
                    transa.Rollback();
                    throw new Exception("No se realizo la transaccion en la Bases de Datos.");
                }
                finally
                {
                    conexion.Close();
                }
            }
        }


    }
}
